/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exo_java_algo_ja42;

/**
 *
 * @author stag
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Instanciement de la classe Voiture pour voiture 1
       Voiture voiture1 = new Voiture("Rouge","Renault", "Megane");
       
       //Instanciement de la classe Voiture pour voiture 2
    Voiture voiture2 = new Voiture("bleue","Peugeot", "208");
       
       //Instanciement de la classe Voiture pour voiture 3
    Voiture voiture3 = new Voiture("Verte","Citroen", "C3");
       //Demarrage des voitures
       
       voiture1.demarrer();
       voiture2.demarrer();
       voiture3.demarrer();
       
       //Avancement des voitures
       
       voiture1.avancer(50);
       voiture2.avancer(20);
       voiture1.avancer(20);
       
       //Affichage des 3 voitures
       
       voiture1.afficher();
       voiture2.afficher();
       voiture3.afficher();
    }
    
}
